#!../../bin/linux-x86_64/dummySMU

#- You may have to change dummySMU to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

epicsEnvSet("SYS", "$(IOC_SYS)")
epicsEnvSet("DEV", "$(IOC_DEV)")


## Register all support components
dbLoadDatabase "dbd/dummySMU.dbd"
dummySMU_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/dummy_smu.db","SYS=$(SYS),DEV =$(DEV), ZNAM=$(ZNAM), ONAM=$(ONAM)")

cd "${TOP}/iocBoot/${IOC}"
iocInit

